package oak.web.demo.handler;

public class CampoObrigatorioException extends BusinessException {
    public CampoObrigatorioException(String campo) {
        super("O campo %s é obrigatorio", campo);
    }

    public CampoObrigatorioException(String mensagem, Object... params) {
        super(mensagem, params);
    }
}
