package oak.web.demo.controller;

import oak.web.demo.model.Usuario;
import oak.web.demo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class UsuarioController {
    @Autowired
    private UsuarioRepository repository;
    @PostMapping("/usuarios")
    public void putUser(@RequestBody Usuario usuario){
        repository.save(usuario);
    }

    @PutMapping("/usuarios")
    public void put(@RequestBody Usuario usuario){
        repository.update(usuario);
    }
    @GetMapping("/usuarios")
    public List<Usuario> getAll(){
        return repository.listAll();
    }
    @GetMapping("/usuario/{id}")
    public Usuario getOne(@PathVariable("id") Integer id){
        return repository.finById(id);
    }
    @DeleteMapping("/usuarios/{id}")
    public void delete(@PathVariable("id") Integer id){
        repository.remove(id);
    }





//    @GetMapping("/usuarios")
//    public List<Usuario>getUsers(){
//        return repository.findAll();
//    }
//
//    @GetMapping("/{username}")
//    public Usuario getUsuario(@PathVariable("username") String username){
//        return repository.findByUsername(username);
//    }
//
//    @DeleteMapping("/{id}")
//    public void deleteUsuario(@PathVariable("id") Integer id){
//        repository.deleteById(id);
//    }
//
//
//
//
//    @PostMapping("")
//    public void postUsuario(@RequestBody Usuario usuario){
//        repository.save(usuario);
//    }

}
